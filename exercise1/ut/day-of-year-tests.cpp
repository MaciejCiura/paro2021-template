#include "gtest/gtest.h"
#include "day-of-year.hpp"

struct DayOfYearTestSuite {};

TEST(DayOfYearTestSuite, dummyTest)
{
    ASSERT_TRUE(true);
}

TEST(DayOfYearTestSuite, January1stIsFitstDayOfYear)
{
    ASSERT_EQ(dayOfYear(1, 1, 2020), 1);
}

TEST(DayOfYearTestSuite, December31stIsLastDayOfNonLeapYear)
{
    ASSERT_EQ(dayOfYear(12, 31, 2021), 365);
}

TEST(DayOfYearTestSuite, December31stIsLastDayOfLeapYear)
{
    ASSERT_EQ(dayOfYear(12, 31, 2020), 366);
}

TEST(DayOfYearTestSuite, Febryary1stIs32ndDayOfYear)
{
    ASSERT_EQ(dayOfYear(2, 1, 2020), 32);
}


