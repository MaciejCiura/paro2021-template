#include "day-of-year.hpp"
#include <array>

int dayOfYear(int month, int dayOfMonth, int year) {

    constexpr std::array<int, 12> daysInMonths{31, 28, 31, 30, 31, 30,
                                               31, 31, 30, 31, 30, 31};
    
    // Leap year
    if((month > 2) && ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0)) {
        dayOfMonth++;
    }
    
    // Sums days of months
    for(int i = 0; i < month-1; i++) {
        dayOfMonth += daysInMonths[i];
    }
    
    return dayOfMonth;
}
