#include "scrabble.hpp"
#include <algorithm>

int calculatePoints(std::string str) {
    int sum = 0;
    std::transform(str.begin(), str.end(), str.begin(), toupper);

    for(char c : str) {
        if(c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U' ||
           c == 'L' || c == 'N' || c == 'R' || c == 'S' || c == 'T') {
            sum += 1;

        } else if(c == 'D' || c == 'G') {
            sum += 2;

        } else if(c == 'B' || c == 'C' || c == 'M' || c == 'P') {
            sum += 3;

        } else if(c == 'F' || c == 'H' || c == 'V' || c == 'W' || c == 'Y') {
            sum += 4;

        } else if(c == 'K') {
            sum += 5;

        } else if(c == 'J' || c == 'X') {
            sum += 8;

        } else if(c == 'Q' || c == 'Z') {
            sum += 10;
        }
    }
    return sum;
}