#include "gtest/gtest.h"
#include "scrabble.hpp"

struct ScrabbleTestSuite {};

TEST(ScrabbleTestSuite, dummyTest)
{
ASSERT_TRUE(true);
}

TEST( ScrabbleTestSuite, CatIs5 )
{
ASSERT_EQ(calculatePoints("cat"), 5);
}

TEST( ScrabbleTestSuite, ButtonIs8 )
{
ASSERT_EQ(calculatePoints("button"), 8);
}

TEST( ScrabbleTestSuite, FishIs10 )
{
ASSERT_EQ(calculatePoints("fish"), 10);
}

TEST( ScrabbleTestSuite, DogIs5 )
{
ASSERT_EQ(calculatePoints("dog"), 5);
}

TEST( ScrabbleTestSuite, CabbageIs14 )
{
ASSERT_EQ(calculatePoints("cabbage"), 14);
}
